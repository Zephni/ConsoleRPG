﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG
{
    class Stat
    {
        private string Name;
        public int Value;
        private int MinValue;
        private int MaxValue;
        private int MaxMaxValue;

        public Stat(string name, int min, int maxmax = 0)
        {
            this.Name = name;
            this.MinValue = min;
            this.MaxMaxValue = maxmax;
            this.MaxValue = this.MaxMaxValue;
        }

        public void SetMaxMaxValue(int value)
        {
            this.MaxMaxValue = value;
        }

        public void SetMaxValue(int value)
        {
            if (value < MinValue) this.MaxValue = MinValue;
            else if (value > MaxMaxValue) this.MaxValue = MaxMaxValue;
            else this.MaxValue = value;
        }

        public void SetValue(int value)
        {
            if(value < MinValue) this.Value = MinValue;
            else if (value > MaxValue) this.Value = MaxValue;
            else this.Value = value;
        }

        public void AddValue(int Value)
        {
            this.SetValue(this.GetValue() + Value);
        }

        public void SubValue(int Value)
        {
            this.SetValue(this.GetValue() - Value);
        }

        public int GetValue()
        {
            return this.Value;
        }

        public int GetMaxValue()
        {
            return this.MaxValue;
        }

        public int GetMaxMaxValue()
        {
            return this.MaxMaxValue;
        }
    }
}
