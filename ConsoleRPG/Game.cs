﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Global;

namespace ConsoleRPG
{
    class Game
    {
        PlayerFighter Player;

        Dictionary<string, EnemyFighter> Enemies;
        List<EnemyFighter[]> EnemyGroups;

        private void AddEnemy(string Name, int Level, List<Move> Moves)
        {
            this.Enemies.Add(Name, new EnemyFighter());
            this.Enemies[Name].Name = Name;
            this.Enemies[Name].SetLevelInit(Level);
            this.Enemies[Name].Moves = Moves;
        }

        public Game()
        {
            MainSetup("");
        }

        public Game(string fileName)
        {
            MainSetup(fileName);
        }

        private void MainSetup(string fileName)
        {
            // Welcome message
            Console.WriteLine("\nWelcome to ConsoleRPG...");
            Console.WriteLine("______________________________________________________________________________\n");

            // Add moves
            Global.Moves.AddMoves();

            // Build player
            if(fileName.Length == 0)
                SetupPlayer();
            else
                SetupPlayer(fileName);

            // Build enemies
            Enemies = new Dictionary<string, EnemyFighter>();

            AddEnemy("Mole", 4, new List<Move> { Global.Moves.List["Attack"] });
            AddEnemy("Magic beaver", 7, new List<Move> { Global.Moves.List["Attack"], Global.Moves.List["Cure"] });
            AddEnemy("Fox", 8, new List<Move> { Global.Moves.List["Attack"], Global.Moves.List["Magic fist"], Global.Moves.List["Cure"] });
            AddEnemy("Sand wyrm", 14, new List<Move> { Global.Moves.List["Attack"], Global.Moves.List["Smash"], Global.Moves.List["Sand blast"] });
            AddEnemy("Kim Yong's Unicorn", 24, new List<Move> { Global.Moves.List["Attack"], Global.Moves.List["Cure"] });
            AddEnemy("Ifrit", 31, new List<Move> { Global.Moves.List["Fire"], Global.Moves.List["Smash"], Global.Moves.List["Cure"] });
            AddEnemy("Hammersmith", 40, new List<Move> { Global.Moves.List["Smash"], Global.Moves.List["Magic hammer"], Global.Moves.List["Cure"] });
            AddEnemy("White wizard", 42, new List<Move> { Global.Moves.List["MP regen"], Global.Moves.List["Cure"], Global.Moves.List["Staff"] });

            AddEnemy("Red XIII", 67, new List<Move> { Global.Moves.List["Smash"], Global.Moves.List["Fire"], Global.Moves.List["MP regen"] });
            AddEnemy("Cloud", 75, new List<Move> { Global.Moves.List["Omnislash"], Global.Moves.List["MP regen"], Global.Moves.List["Cure"], Global.Moves.List["Smash"] });
            
            AddEnemy("Cloud Ultimate", 99, new List<Move> { Global.Moves.List["Omnislash"], Global.Moves.List["MP regen"], Global.Moves.List["Cure"], Global.Moves.List["Smash"] });

            // Loop
            bool Running = true;
            while (Running)
            {
                int Option = ChooseAnOption();

                if (Option == 1)
                    SeeStats();
                else if (Option == 2)
                    ChooseEnemy();
                else if (Option == 3)
                    Save();
                else if (Option == 4)
                    Running = false;
                else if (Option == 34)
                    SetEXP();
            }
        }

        private void SetupPlayer()
        {
            Player = new PlayerFighter();
            Player.Name = ZConsole.AskString("Please choose your fighters name");
            Player.SetLevelInit(7);
        }

        private void SetupPlayer(string fileName)
        {

            string[] lines = System.IO.File.ReadAllLines(Program.SaveFile);

            Player = new PlayerFighter();
            Player.Name = lines[0];
            Player.SetLevelInit(7);
            Player.SetEXP(Convert.ToInt32(lines[1]));

            string[] moves = lines[2].Split(',');
            Move tryadd = new Move();
            foreach (string move in moves)
            {
                tryadd = Global.Moves.List[move];
                if (!Player.Moves.Contains(tryadd))
                    Player.Moves.Add(tryadd);
            }
        }

        static public void Wait(int Amount)
        {
            System.Threading.Thread.Sleep(Amount);
        }

        public int ChooseAnOption()
        {
            Console.WriteLine("");
            Console.WriteLine("----------------");
            Console.WriteLine("|     GAME     |");
            Console.WriteLine("----------------");
            Console.WriteLine("");
            Console.WriteLine("1. See " + Player.Name + "'s stats");
            Console.WriteLine("2. Fight an enemy");
            Console.WriteLine("3. Save");
            Console.WriteLine("4. Back to main menu");
            return ZConsole.AskInt("\nSelect an option", new int[] {1, 2, 3, 4, 34});
        }

        public void SetEXP()
        {
            Console.WriteLine("");
            Console.WriteLine("------------------------");
            Console.WriteLine("|     Setting EXP     |");
            Console.WriteLine("-----------------------");
            Console.WriteLine("");
            Console.WriteLine("Current Level: " + Player.GetLevel() + ", EXP: " + Player.GetEXP());

            Player.SetEXP(ZConsole.AskInt("\nEnter EXP ammount"));
        }

        /*private EnemyFighter NewEnemyInstance(EnemyFighter origional)
        {
            EnemyFighter enemy = new EnemyFighter();
            enemy.Name = origional.Name;
            enemy.SetLevel(origional.GetLevel());
            return enemy;
        }*/

        public void ChooseEnemy()
        {
            // Enemy groups
            EnemyGroups = new List<EnemyFighter[]>();

            EnemyGroups.Add(new EnemyFighter[] { Enemies["Mole"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Magic beaver"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Fox"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Mole"].GetInstance(), Enemies["Magic beaver"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Sand wyrm"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Sand wyrm"].GetInstance(), Enemies["Fox"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Kim Yong's Unicorn"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Ifrit"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Hammersmith"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Hammersmith"].GetInstance(), Enemies["Ifrit"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Hammersmith"].GetInstance(), Enemies["White wizard"] });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Ifrit"].GetInstance(), Enemies["Kim Yong's Unicorn"].GetInstance(), Enemies["Hammersmith"].GetInstance()});
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Red XIII"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Cloud"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Cloud"].GetInstance(), Enemies["Red XIII"].GetInstance() });
            EnemyGroups.Add(new EnemyFighter[] { Enemies["Cloud Ultimate"].GetInstance(), Enemies["Red XIII"].GetInstance() });

            Console.WriteLine("");
            Console.WriteLine("---------------------------");
            Console.WriteLine("|     CHOOSE AN ENEMY     |");
            Console.WriteLine("--------------------------");
            Console.WriteLine("");

            List<int> Options = new List<int>();
            int I = 0;

            Options.Add(I);
            Console.WriteLine(I.ToString() + ". < Go back");
            I++;

            List<string> fighterNames = new List<string>();
            foreach (EnemyFighter[] fighters in EnemyGroups)
            {
                Console.Write(I.ToString() + ": ");
                foreach (EnemyFighter fighter in fighters)
                {
                    fighterNames.Add(fighter.Name+" (L"+fighter.GetLevel()+")");
                }
                Console.Write(String.Join(" and ", fighterNames));
                Console.Write("\n");

                fighterNames = new List<string>();

                Options.Add(I);
                I++;
            }

            int Option = ZConsole.AskInt("\nChoose an enemy", Options.ToArray());

            if (Option != 0)
                Battle(EnemyGroups[Option - 1].ToList<EnemyFighter>());
        }

        public void Save()
        {
            string[] lines = new string[3];
            lines[0] = Player.Name;
            lines[1] = Player.GetEXP().ToString();

            string moves = "";
            if (Player.Moves.Count() > 0)
            {
                List<string> pMoves = new List<string>();
                foreach (Move pMove in Player.Moves)
                    pMoves.Add(pMove.Name);
                moves = String.Join(",", pMoves);
            }

            lines[2] = moves;

            System.IO.File.WriteAllLines(Program.SaveFile, lines);

            Console.WriteLine("\n------------------------------------------------------------------------------------");
            Console.WriteLine("\n                                  Saved the game!\n");
            Console.WriteLine("------------------------------------------------------------------------------------");
        }

        public void Battle(List<EnemyFighter> EnemyGroup)
        {
            Console.WriteLine("");
            Console.WriteLine("----------------------------");
            Console.WriteLine("|     BEGINING BATTLE     |");
            Console.WriteLine("---------------------------");
            Console.WriteLine("");

            List<string> fighterNames = new List<string>();
            int EXPGains = 0;
            List<Move> EnemeyMoves = new List<Move>();
            foreach (Fighter fighter in EnemyGroup)
            {
                fighterNames.Add(fighter.Name);
                EXPGains += Convert.ToInt32((fighter.GetLevel()/4) * fighter.GetLevel() * 1.6) + 8;

                EXPGains += (fighter.GetLevel() * fighter.GetLevel()) / (11 - (fighter.GetLevel()/10));

                foreach(Move move in fighter.Moves){
                    if (!EnemeyMoves.Contains(move))
                    {
                        Random chance = new Random();
                        if(chance.Next(0, 4) == 0) // 1 in 4 chance of learning move
                            EnemeyMoves.Add(move);
                    }
                        
                }
            }
            if (EnemyGroup.Count() > 1)
                EXPGains *= Convert.ToInt32((EnemyGroup.Count() * 0.8) + 0.3);

            Console.WriteLine("Decisive battle with " + Player.Name + " vs " + String.Join(" and ", fighterNames));

            Game.Wait(500);

            int BattleStatus = 1; // 0. Player dead, 1. Normal, 2. All enemies dead

            while (BattleStatus == 1)
            {
                Player.TakeTurn(ref Player, ref EnemyGroup);

                BattleStatus = CheckBattle(EnemyGroup);

                for (int I = 0; I < EnemyGroup.Count; I++ )
                {
                    if (BattleStatus == 1)
                    {
                        EnemyGroup[I].TakeTurn(ref Player, ref EnemyGroup);
                        BattleStatus = CheckBattle(EnemyGroup);
                    }
                }
            }

            if (BattleStatus == 0)
            {
                Console.WriteLine("\nGame over!\n");
                Player.FillStats();
                Console.ReadLine();
                Environment.Exit(0);
            }
                

            if (BattleStatus == 2)
            {
                List<string> NewMoves = new List<string>();
                foreach (Move move in EnemeyMoves)
                {
                    if (!Player.Moves.Contains(move)){
                        if(move.CanLearn == true)
                        {
                            Player.Moves.Add(move);
                            NewMoves.Add(move.Name);
                        }
                    }
                }

                int CurLevel = Player.GetLevel();
                Player.AddEXP(EXPGains);
                Console.WriteLine("\n\nPlayer won the battle!");
                Game.Wait(100);
                Console.WriteLine("--------------------------------------------------");
                Game.Wait(100);
                Console.WriteLine("EXP earned: "+EXPGains);
                Game.Wait(100);

                if (Player.GetLevel() > CurLevel)
                {
                    Console.WriteLine(Player.Name + " IS NOW LEVEL " + Player.GetLevel() + "!");
                    Game.Wait(100);
                }
                
                if(NewMoves.Count() > 0)
                {
                    Console.WriteLine("NEW MOVES LEARNED: "+String.Join(", ", NewMoves)+"\n");
                }

                SeeStats();
                Game.Wait(100);
                Console.WriteLine("--------------------------------------------------");
            }

            Console.ReadKey();
        }

        public int CheckBattle(List<EnemyFighter> EnemyGroup)
        {
            if (Player.HP.GetValue() <= 0)
            {
                Console.WriteLine(Player.Name + " died!");
                Game.Wait(1000);
                return 0;
            }

            EnemyFighter enemy = new EnemyFighter();
            for(int I = 0; I < EnemyGroup.Count(); I++)
            {
                enemy = EnemyGroup[I];
                if (enemy.HP.GetValue() <= 0)
                {
                    EnemyGroup.RemoveAt(I);

                    if (EnemyGroup.Count <= 0)
                    {
                        Console.WriteLine("\n" + enemy.Name + " perished, all enemies defeated!\n");
                        Game.Wait(1500);
                        return 2;
                    }
                    else
                    {
                        Console.WriteLine(enemy.Name + " perished!");
                        Game.Wait(1000);
                    }
                }
            }

            return 1;
        }

        public void SeeStats()
        {
            int EXPLeft = ((Program.EXP.GetLevelExp(Player.GetLevel()+1) - Player.GetEXP()));
            Console.WriteLine("");
            Game.Wait(100);
            Console.WriteLine("|     STATS     |");
            Game.Wait(100);
            Console.WriteLine("");
            Game.Wait(100);
            Console.WriteLine("Level: " + Player.GetLevel());
            Game.Wait(100);
            Console.WriteLine("EXP: " + Player.GetEXP() + " / " + Program.EXP.GetLevelExp(Player.GetLevel() + 1) + " - next level: " + EXPLeft);
            Game.Wait(100);
            if (Player.Moves.Count() > 0)
            {
                List<string> pMoves = new List<string>();
                foreach (Move pMove in Player.Moves)
                    pMoves.Add(pMove.Name);
                Console.WriteLine("Moves: " + String.Join(", ", pMoves));
            }
            Game.Wait(100);
            Console.WriteLine("");
            Game.Wait(100);

            Console.Write("" + "HP");
            Console.Write("\t" + "MP");
            Console.Write("\t" + "ATK");
            Console.Write("\t" + "MATK");
            Console.Write("\t" + "DEF");
            Console.Write("\t" + "MDEF");
            Game.Wait(100);
            Console.WriteLine("");
            Game.Wait(100);

            Console.Write("" + Player.HP.GetValue());
            Console.Write("\t" + Player.MP.GetValue());
            Console.Write("\t" + Player.ATK.GetValue());
            Console.Write("\t" + Player.MATK.GetValue());
            Console.Write("\t" + Player.DEF.GetValue());
            Console.Write("\t" + Player.MDEF.GetValue());
            Game.Wait(100);
            Console.WriteLine("");
        }
    }
}
