﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Global;

namespace ConsoleRPG
{
    class Fighter
    {
        // Main
        public string Name = "";
        private int Level = 0;
        private int EXP = 0;
        public List<Move> Moves;

        // Stats
        public Stat HP = new Stat("HP", 0, 99999);
        public Stat MP = new Stat("MP", 0, 999);
        public Stat ATK = new Stat("Attack", 0, 255);
        public Stat MATK = new Stat("Magic attack", 0, 255);
        public Stat DEF = new Stat("Defence", 0, 255);
        public Stat MDEF = new Stat("Magic defence", 0, 255);

        public Fighter()
        {
            
        }

        public void SetLevel(int I)
        {
            if (I < 0) this.Level = 0;
            else if (I > Program.EXP.GetMaxLevels()) this.Level = Program.EXP.GetMaxLevels();
            else this.Level = I;
            SetLevelValues(this.Level);
            FillStats();
        }

        public void SetLevelInit(int I)
        {
            SetLevel(I);
            this.EXP = Program.EXP.GetLevelExp(I);
        }

        public int GetLevel(){
            return this.Level;
        }

        public void SetEXP(int Amt)
        {
            this.EXP = Amt;
            this.SetLevel(Program.EXP.GetLevelByEXP(this.EXP));
        }

        public int GetEXP()
        {
            return this.EXP;
        }

        public void AddEXP(int Amt)
        {
            this.SetEXP(this.GetEXP() + Amt);
        }

        public void SetLevelValues(int I)
        {
            //this.HP.SetMaxValue(Convert.ToInt32(Math.Floor(1500 * (Math.Pow(1 + 0.021, I))) - 1500) + 40);
            this.HP.SetMaxValue(Convert.ToInt32(Math.Floor(1500 * (Math.Pow(1 + 0.025, I * (1.2+(Math.Abs(60-I)/80))))) - 1600));
            this.MP.SetMaxValue(Convert.ToInt32(Math.Floor(150 * (Math.Pow(1 + 0.021, I))) - 150) + 4);
            this.ATK.SetMaxValue(Convert.ToInt32(Math.Floor(80 * (Math.Pow(1 + 0.02023, I))) - 70) / 2);
            this.MATK.SetMaxValue(Convert.ToInt32(Math.Floor(80 * (Math.Pow(1 + 0.02023, I))) - 70) / 2);
            this.DEF.SetMaxValue(Convert.ToInt32(Math.Floor(80 * (Math.Pow(1 + 0.02023, I))) - 70) / 2);
            this.MDEF.SetMaxValue(Convert.ToInt32(Math.Floor(80 * (Math.Pow(1 + 0.02023, I))) - 70) / 2);                
        }

        public void FillStats()
        {
            this.HP.SetValue(this.HP.GetMaxValue());
            this.MP.SetValue(this.MP.GetMaxValue());
            this.ATK.SetValue(this.ATK.GetMaxValue());
            this.MATK.SetValue(this.MATK.GetMaxValue());
            this.DEF.SetValue(this.DEF.GetMaxValue());
            this.MDEF.SetValue(this.MDEF.GetMaxValue());
        }
    }
}
