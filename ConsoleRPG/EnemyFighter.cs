﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Global;

namespace ConsoleRPG
{
    class EnemyFighter : Fighter
    {
        public EnemyFighter()
        {
            Moves = new List<Move>();
        }

        public EnemyFighter GetInstance()
        {
            EnemyFighter NewInstance = new EnemyFighter();
            NewInstance.Name = this.Name;
            NewInstance.SetLevelInit(this.GetLevel());
            NewInstance.Moves = this.Moves;
            return NewInstance;
        }

        public void TakeTurn(ref PlayerFighter Player, ref List<EnemyFighter> EnemyGroup)
        {
            Console.WriteLine("\n"+this.Name+"'s turn...");

            Random random = new Random();

            bool AcceptMove = false;
            int tryit = 0;
            Move move = this.Moves[random.Next(0, this.Moves.Count())];            

            while(AcceptMove == false)
            {
                tryit = random.Next(1, 5);
                if (move.MP > this.MP.GetValue() && tryit != 1) // One in 5 chance of making a failed move
                {
                    move = this.Moves[random.Next(0, this.Moves.Count())];
                }
                else
                {
                    AcceptMove = true;
                    if (move.Offensive)
                        move.Action(this, Player);
                    else
                    {
                        int min = EnemyGroup.Min(i => i.HP.GetValue());
                        EnemyFighter target = EnemyGroup.First(x => x.HP.GetValue() == min);
                        move.Action(this, target);
                    }
                }
            }
            

            Game.Wait(1000);
        }
    }
}
