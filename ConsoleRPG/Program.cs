﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG
{
    class Program
    {
        static public Exp EXP;
        static bool Running = true;

        static public string SaveFile = @"C:\Users\Public\ConsoleRPGSave.txt";

        static void Main(string[] args)
        {
            EXP = new Exp(99);

            while(Running)
            {
                int Choice = ZConsole.AskInt("\nWelcome to ConsoleRPG...\n\n1. Begin game\n2. Load\n3. See EXP table\n4. Quit\n\nSelect an option", new int[] { 1, 2, 3, 4 });

                if (Choice == 1)
                    RunGame();
                else if (Choice == 2)
                    Load();
                else if (Choice == 3)
                    EXPList();
                else if (Choice == 4)
                    Running = false;
            }
            
        }

        static void RunGame()
        {
            Game game = new Game();
            Console.WriteLine("");
            Running = false;
        }

        static void Load()
        {
            if(System.IO.File.Exists(Program.SaveFile))
            {
                Game game = new Game(Program.SaveFile);
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine("\n\nNo save file exists, save one first...\n");
            }
        }

        static void EXPList()
        {
            Exp EXP = new Exp(99);

            Console.WriteLine("Level" + " \t\t " + "Total EXP" + " \t " + "Difference");

            for (int I = 0; I <= EXP.GetMaxLevels(); I++)
            {
                Console.WriteLine(I + " \t\t " + EXP.GetLevelExp(I) + " \t\t " + EXP.GetLevelDiffExp(I));
            }
        }

        public void SeeStats()
        {
            Fighter Player = new Fighter();

            Console.Write("Level");
            Console.Write("\t" + "HP");
            Console.Write("\t" + "MP");
            Console.Write("\t" + "ATK");
            Console.Write("\t" + "MATK");
            Console.Write("\t" + "DEF");
            Console.Write("\t" + "MDEF");
            Console.WriteLine("");

            for (int I = 1; I <= EXP.GetMaxLevels(); I++)
            {
                Player.SetLevel(I);
                Console.Write(I);
                Console.Write("\t" + Player.HP.GetValue());
                Console.Write("\t" + Player.MP.GetValue());
                Console.Write("\t" + Player.ATK.GetValue());
                Console.Write("\t" + Player.MATK.GetValue());
                Console.Write("\t" + Player.DEF.GetValue());
                Console.Write("\t" + Player.MDEF.GetValue());
                Console.WriteLine("");
            }
        }
    }
}
