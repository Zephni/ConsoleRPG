﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG
{
    public class Exp
    {
        private int[] Levels;
        private int[] Diff;
        private int MaxLevels;

        public Exp(int _MaxLevels = 99)
        {
            this.MaxLevels = _MaxLevels;
            this.Levels = new int[this.MaxLevels+1];
            this.Diff = new int[this.MaxLevels+1];

            this.Levels[0] = 0;

            for (int I = 1; I <= this.MaxLevels; I++)
            {
                double val = Math.Floor(450 * (Math.Pow(1 + 0.1, I))) - 450;

                this.Levels[I] = Convert.ToInt32(val);

                if(I >= 0 && I <= this.MaxLevels+1)
					this.Diff[I] = this.Levels[I] - this.Levels[I-1];
				else
					this.Diff[I] = 0;
            }
        }

        public int GetLevelByEXP(int _EXP)
        {
            int lvl = 0;
            for (int I = 1; I <= this.MaxLevels; I++)
                if (_EXP >= this.Levels[I])
                    lvl = I;

            return lvl;
        }

        public int GetLevelExp(int I)
        {
            I = Math.Max(1, Math.Min(99, I));
		    return this.Levels[I];
        }

        public int GetLevelDiffExp(int I)
        {
            I = Math.Max(1, Math.Min(99, I));
			return this.Diff[I];
        }

        public int GetMaxLevels(){
			return this.MaxLevels;
		}
    }
}
