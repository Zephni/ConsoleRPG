﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Global;

namespace ConsoleRPG
{
    class PlayerFighter : Fighter
    {
        public PlayerFighter()
        {
            Moves = new List<Move>();
            Moves.Add(Global.Moves.List["Attack"]);
            Moves.Add(Global.Moves.List["Aura beam"]);
        }

        public void TakeTurn(ref PlayerFighter Player, ref List<EnemyFighter> EnemyGroup)
        {
            Console.WriteLine("\n\n"+Player.Name+"'s turn: ");

            Dictionary<int, Fighter> Targets = new Dictionary<int, Fighter>();
            int I = 0;
            Targets.Add(0, Player);
            foreach (EnemyFighter enemy in EnemyGroup)
            {
                I++;
                Targets.Add(I, enemy);
            }

            foreach (KeyValuePair<int, Fighter> Target in Targets)
            {
                Console.Write("\n"+Target.Value.Name + " ");
                Console.Write("(HP: " + Target.Value.HP.GetValue() + "/" + Target.Value.HP.GetMaxValue() + ", ");
                Console.Write("(MP: " + Target.Value.MP.GetValue() + "/" + Target.Value.MP.GetMaxValue() + ")");
            }

            Console.WriteLine("");
            Game.Wait(300);

            Console.WriteLine("");
            int M = 0;
            List<int> moves = new List<int>();
            foreach(Move move in Moves)
            {
                M++;
                moves.Add(M);
                Console.Write("\n"+M+". "+move.Name);
                if (move.MP > 0)
                    Console.Write(" ("+move.MP+"mp)");
            }
            Console.WriteLine("");
            int Opt1 = ZConsole.AskInt("\nChoose an option", moves.ToArray());

            foreach (KeyValuePair<int, Fighter> Target in Targets)
                Console.Write("\n"+Target.Key + ". " + Target.Value.Name + " ");

            int targetOption = ZConsole.AskInt("\n\nChoose a target", Targets.Keys.ToArray<int>());

            Moves[Opt1 - 1].Action(Player, Targets[targetOption]);
            
            Game.Wait(1000);
        }
    }
}
