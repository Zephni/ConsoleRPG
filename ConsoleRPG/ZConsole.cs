﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG
{
    static public class ZConsole
    {
        static public int AskInt(string Question, int[] Available = null)
        {
            Available = Available ?? new int[0];

            while(true)
            {
                Console.Write(Question + ": ");
                
                int Value = 0;
                if(int.TryParse(Console.ReadLine(), out Value))
                    if(Available.Contains<int>(Value) || Available.Count() == 0)
                        return Value;
            }
        }

        static public string AskString(string Question, string[] Available = null)
        {
            Available = Available ?? new string[0];

            while(true)
            {
                Console.Write(Question + ": ");
                string Value = Convert.ToString(Console.ReadLine());

                if (Available.Contains<string>(Value) || Available.Count() == 0)
                    return Value;
            }
        }
    }
}
