﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Global;

namespace ConsoleRPG.MovesList
{
    class Move_Attack : Move
    {
        public Move_Attack()
        {
            this.Name = "Attack";
            this.DmgMultiplier = 1;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            target.HP.SubValue(Damage);
            Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
        }
    }

    class Move_Staff : Move
    {
        public Move_Staff()
        {
            this.Name = "Staff";
            this.DmgMultiplier = 0.5;
            this.CanLearn = false;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            target.HP.SubValue(Damage);
            Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
        }
    }

    class Move_Smash : Move
    {
        public Move_Smash()
        {
            this.Name = "Smash";
            this.DmgMultiplier = 1.5;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            target.HP.SubValue(Damage);
            Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");

            user.HP.SubValue(Damage / 4);
            Console.WriteLine("\n" + user.Name + " damages themself during the smash and takes "+(Damage / 4)+" damage");
        }
    }

    class Move_MPRegen : Move
    {
        public Move_MPRegen()
        {
            this.Name = "MP regen";
            this.DmgMultiplier = 3.8;
            this.MP = 0;
            this.Offensive = false;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = user.MP.GetValue()/4;

            target.MP.AddValue(Damage);
            Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " regenerates " + Damage + " MP");
        }
    }

    class Move_MagicHammer : Move
    {
        public Move_MagicHammer()
        {
            this.Name = "Magic hammer";
            this.DmgMultiplier = 4;
            this.MP = 160;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_Fire : Move
    {
        public Move_Fire()
        {
            this.Name = "Fire";
            this.DmgMultiplier = 3.2;
            this.MP = 100;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_Aura : Move
    {
        public Move_Aura()
        {
            this.Name = "Aura beam";
            this.DmgMultiplier = 1.33;
            this.MP = 10;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_MagicFist : Move
    {
        public Move_MagicFist()
        {
            this.Name = "Magic fist";
            this.DmgMultiplier = 2;
            this.MP = 22;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_SandBlast : Move
    {
        public Move_SandBlast()
        {
            this.Name = "Sand blast";
            this.DmgMultiplier = 2.3;
            this.MP = 35;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and dealt " + Damage + " damage");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_Cure : Move
    {
        public Move_Cure()
        {
            this.Name = "Cure";
            this.DmgMultiplier = 2;
            this.MP = 25;
            this.Offensive = false;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            int Damage = Std_Damage(user);
            if (user.MP.GetValue() >= this.MP)
            {
                user.MP.SubValue(this.MP);
                target.HP.AddValue(Damage);
                Console.WriteLine("\n" + user.Name + " uses " + this.Name + " on " + target.Name + " and cures " + Damage);
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }

    class Move_Omnislash : Move
    {
        public Move_Omnislash()
        {
            this.Name = "Omnislash";
            this.DmgMultiplier = 0.29;
            this.MP = 350;
            Moves.List.Add(this.Name, this);
        }

        public override void Action(Fighter user, Fighter target)
        {
            if (user.MP.GetValue() >= this.MP)
            {
                Console.WriteLine("\n" + user.Name + " unleashes " + this.Name + " on " + target.Name + "!");
                int Damage = Std_Damage(user);
                user.MP.SubValue(this.MP);

                for (int I = 1; I <= 14; I++)
                {
                    Damage = Std_Damage(user);
                    target.HP.SubValue(Damage);
                    Console.WriteLine("\n" + " Slash! "+Damage+" damage!");
                    Game.Wait(200);
                }
                Game.Wait(800);
                Damage = Std_Damage(user) * 3;
                target.HP.SubValue(Damage);
                Console.WriteLine("\n" + " Final blow causes " + Damage + " damage!");
            }
            else
            {
                Console.WriteLine("\n" + user.Name + " does not have enough MP to use '" + this.Name + "'");
            }
        }
    }
}
