﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ConsoleRPG.Global
{
    class Move
    {
        public string Name;
        public double DmgMultiplier;
        public int MP;
        public bool Offensive = true;
        public bool CanLearn = true;

        public virtual void Action(Fighter user, Fighter target)
        {
            Console.Write("Undefined action...");
        }

        public int Std_Damage(Fighter user)
        {
            Random random = new Random();
            int DmgInit = Convert.ToInt32((user.ATK.GetValue() * (3.8 * Math.Max(user.GetLevel() / 38, 1))) * DmgMultiplier) + 14;
            int Damage = random.Next(Convert.ToInt32(DmgInit * 0.95), Convert.ToInt32(DmgInit * 1.05));
            return Damage;
        }
    }

    public class Methods
    {
        public Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }
    }


    static class Moves
    {

        static public Dictionary<string, Move> List = new Dictionary<string, Move>();

        static public void AddMoves()
        {
            Methods methods = new Methods();
            Type[] typelist = methods.GetTypesInNamespace(Assembly.GetExecutingAssembly(), "ConsoleRPG.MovesList");
            for (int i = 0; i < typelist.Length; i++)
            {
                Activator.CreateInstance(Type.GetType("ConsoleRPG.MovesList." + typelist[i].Name));
            }
        }
    }
}
